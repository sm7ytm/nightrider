void setup() {
  // Här skriver vi inställningar som görs vid första start, inget för tillfället

}


// Här skapas den multidimensionella array som gör mönstret
// Eftersom det är sex lysdioder som är kopplade till Arduinon
// blir det sex kolumnder med ett pwm-värde för varje led

// Varje rad motsvarar ett tillstånd för samtliga sex lysdioder
// vid varje givet tillfälle

int pattern[20][6] = { 
    {252, 0, 0, 0, 0, 0}, 
    {189, 252, 0, 0, 0, 0},
    {126, 189, 252, 0, 0, 0},
    {63, 126, 189, 252, 0, 0},
    {0, 63, 126, 189, 252, 0},
    {0, 0, 63, 126, 189, 252},
    {0, 0, 0, 63, 126, 189},
    {0, 0, 0, 0, 63, 126},
    {0, 0, 0, 0, 0, 63},
    {0, 0, 0, 0, 0, 252},
    {0, 0, 0, 0, 252, 189},
    {0, 0, 0, 252, 189, 126},
    {0, 0, 252, 189, 126, 63},
    {0, 252, 189, 126, 63, 0},
    {252, 189, 126, 63, 0, 0},
    {189, 126, 63, 0, 0, 0},
    {126, 63, 0, 0, 0, 0},
    {63, 0, 0, 0, 0, 0}
    };


// Här skapar vi en array för dom specifika utgångarna och tack vare detta 
// kan vi använda indexet att räkna med i en loop
int led_pin[6] = {3, 5, 6, 9, 10, 11};

// Här kör själva programmet, detta snurrar hela tiden
void loop() {

// Yttre loopen som snurrar radvis för att stega fram respektive mönster
for (int rad = 0; rad < 18; rad++) {         

  // Inre loop som stegar fram respektive led inom varje rad/mönster
  for (int led = 0; led < 6; led++) { 

    // Här tänder vi en given led, angiven av dels vilket varv i inre loopen vi
    // är på, och dels vilken styrka som ska användas enligt den
    // multidimensionella arrayen                
    analogWrite(led_pin[led], pattern[rad][led]);  

    // en delay på 10 millisekunder är lagom för att ge rätt "flyt" i skiftena    
    delay(10);                                        
    }                                                   

  }
}
